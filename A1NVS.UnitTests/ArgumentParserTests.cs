using NUnit.Framework;
using A1NVS;

namespace A1NVS.UnitTests
{
    public class ArgumentParserTests
    {
        private ArgumentParser _argumentParser;

        [SetUp]
        public void Setup()
        {
            _argumentParser = new ArgumentParser();
        }

        [Test]
        public void tryParseArray_AreEqual()
        {
            string[] inputArgs = {"2", "3", "5", "4", "q", "3"};
            int[] successArgs = {2, 3, 5, 4, 3};

            _argumentParser.TryParse(inputArgs);

            Assert.AreEqual(Container.Numbers.ToArray(), successArgs);
        }

        [Test]
        public void tryParseEmpty_True()
        {
            string[] inputArgs = {};

            _argumentParser.TryParse(inputArgs);

            Assert.AreEqual(Container.Numbers.Count, 20);
        }
    }
}