using System;
using System.Collections.Generic;
using NUnit.Framework;
using A1NVS;

namespace A1NVS.UnitTests
{
    public class ContainerTests
    {
        [Test]
        public void numbersInstanceof_isInstanceOf()
        {
            var numbers = Container.Numbers;

            Assert.IsInstanceOf(typeof(List<int>), numbers);
        }

        [Test]
        public void secureRandomInstanceOf_isInstanceOf()
        {
            var secureRandom = Container.GetSecureRandom();
            
            Assert.IsInstanceOf(typeof(SecureRandom), secureRandom);
        }

        [Test]
        public void presenterInstanceOf_isInstanceOf()
        {
            var presenter = Container.GetPresenter();
            
            Assert.IsInstanceOf(typeof(Presenter), presenter);
        }
    }
}