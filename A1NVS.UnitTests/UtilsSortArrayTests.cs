﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using A1NVS;

namespace A1NVS.UnitTests
{
    class UtilsSortArrayTests
    {
        private int[] input = {9, 3, 109, -3, 2, 4, -99};

        private int[] success = {-99, -3, 2, 3, 4, 9, 109};
        
        private int[] success2 = {109, 9, 4, 3, 2, -3, -99};

        [Test]
        public void shakerSort_True()
        {
            Utils.SortArray sortArray = Container.GetSortArray();

            List<int> sort = sortArray.Shaker(new List<int>(input));
            Assert.AreEqual(sort.ToArray(), success2);
        }

        [Test]
        public void shakerSort_False()
        {
            Utils.SortArray sortArray = Container.GetSortArray();

            List<int> sort = sortArray.Shaker(new List<int>(input));
            Assert.AreNotEqual(sort.ToArray(), input);
        }

        [Test]
        public void bubbleSort_True()
        {
            Utils.SortArray sortArray = Container.GetSortArray();
            List<int> sort = sortArray.Bubble(new List<int>(input));

            Assert.AreEqual(sort.ToArray(), success);
        }

        [Test]
        public void bubbleSort_False()
        {
            Utils.SortArray sortArray = Container.GetSortArray();

            List<int> sort = sortArray.Bubble(new List<int>(input));
            Assert.AreNotEqual(sort.ToArray(), input);
        }

        [Test]
        public void selectionSort_True()
        {
            Utils.SortArray sortArray = Container.GetSortArray();
            List<int> sort = sortArray.Selection(new List<int>(input));

            Assert.AreEqual(sort.ToArray(), success);
        }

        [Test]
        public void selectionSort_False()
        {
            Utils.SortArray sortArray = Container.GetSortArray();
            List<int> sort = sortArray.Selection(new List<int>(input));

            Assert.AreNotEqual(sort.ToArray(), input);
        }
    }
}