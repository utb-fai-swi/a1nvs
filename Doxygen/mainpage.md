## Projekt pro práci s čísly

Tento projekt je věnován práci s čísly.

### Autoři

* Radka Zaoralová
* Marek Novák
* Filip Šedivý

K projektu byl připojen i **Nikolas Sucháček** který nás po testu z databází navždy opustil. Jeho **podíl na práci lze vyjádřit na celém čísle** jehož hodnota činní **0**. 

```csharp
// Nikolasův podíl vyjádřen pomocí C#
int NikolasPodil = 0;
Console.WriteLine(NikolasPodil);
```