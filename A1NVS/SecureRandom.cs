using System;
using System.Security.Cryptography;

namespace A1NVS
{
    /// <summary>
    /// Pseudo-random generator
    /// </summary>
    public class SecureRandom
    {
        private readonly RandomNumberGenerator _rng;

        public SecureRandom()
        {
            _rng = new RNGCryptoServiceProvider();
        }

        /// <summary>
        /// Random numbers are being generated based on the basic seed of 8 bytes being generated.
        /// Subsequently, this seed is used to generate a random number.
        /// </summary>
        /// <param name="min">Minimum value generated</param>
        /// <param name="max">Maximum generated value</param>
        /// <returns>Generated number</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public int RandomInteger(int min, int max)
        {
            if (min > max)
            {
                throw new ArgumentOutOfRangeException(nameof(min));
            }

            if (min == max)
            {
                return min;
            }

            byte[] randomBytes = new byte[8];
            _rng.GetBytes(randomBytes);
            int generatedValue = Math.Abs(BitConverter.ToInt32(randomBytes, startIndex: 0));

            int diff = max - min;
            int mod = generatedValue % diff;
            int normalizedNumber = min + mod;

            return normalizedNumber;
        }
    }
}