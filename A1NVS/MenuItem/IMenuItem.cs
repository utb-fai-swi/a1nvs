namespace A1NVS.MenuItem
{
    /// <summary>
    /// Interface for field item
    /// </summary>
    public interface IMenuItem
    {
        void Main();
    }
}