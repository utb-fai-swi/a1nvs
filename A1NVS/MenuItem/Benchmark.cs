using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using A1NVS.Utils;
using Microsoft.VisualBasic;

namespace A1NVS.MenuItem
{
    /// <summary>
    ///  Menu item - benchamark short algorithms
    /// </summary>
    public class Benchmark : IMenuItem
    {
        private Utils.SortArray _sortArray;

        /// <summary>
        /// Enum of algoritmus
        /// </summary>
        private enum Algo
        {
            Shaker,
            Bubble,
            Selection
        };

        /// <summary>
        /// Constructor
        /// </summary>
        public Benchmark()
        {
            _sortArray = Container.GetSortArray();
        }

        /// <summary>
        /// Start method of menu item
        /// </summary>
        public void Main()
        {
            Output.Info("Spouštím benchmark ladících algoritmů ve 3 nových vláknech");

            Task shaker = Task.Factory.StartNew(() => RunTask(Algo.Shaker));
            Task bubble = Task.Factory.StartNew(() => RunTask(Algo.Bubble));
            Task selection = Task.Factory.StartNew(() => RunTask(Algo.Selection));
            
            Task.WaitAll(shaker, bubble, selection);
        }

        /// <summary>
        /// Run async task
        /// </summary>
        /// <param name="algo">Type of algoritm</param>
        private void RunTask(Algo algo)
        {
            Thread.Yield();
            Stopwatch _stopwatch = Stopwatch.StartNew();

            switch (algo)
            {
                case Algo.Bubble:
                    _sortArray.Bubble(Container.Numbers);
                    break;

                case Algo.Selection:
                    _sortArray.Selection(Container.Numbers);
                    break;

                case Algo.Shaker:
                    _sortArray.Shaker(Container.Numbers);
                    break;
            }

            _stopwatch.Stop();

            string watchElapsed = _stopwatch.Elapsed.TotalMilliseconds > 1000
                ? $"{Math.Round(_stopwatch.Elapsed.TotalSeconds, 2)}s"
                : $"{Math.Round(_stopwatch.Elapsed.TotalMilliseconds, 2)}ms";

            int spaces = 12 - algo.ToString().Length;
            Output.Info($"Algoritmus: {algo.ToString()}{new string(' ', spaces)} doba řazení: {watchElapsed}");
        }
    }
}