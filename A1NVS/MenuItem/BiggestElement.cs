using System;

namespace A1NVS.MenuItem
{
    /// <summary>
    /// Menu item - biggest element
    /// </summary>
    public class BiggestElement : IMenuItem
    {
        /// <summary>
        /// Finding biggest element
        /// </summary>

        public void Main()
        {
            Console.WriteLine("Nejvetsi cislo");

            int maxNumber = Container.Numbers[0];
            int positionNumber = 0;

            if (Container.Numbers.Count > 1)
            {
                for (int i = 1; i < Container.Numbers.Count; i++)
                {
                    int actualNumber = Container.Numbers[i];

                    if (actualNumber > maxNumber)
                    {
                        maxNumber = actualNumber;
                        positionNumber = i;
                    }
                }
            }

            Console.WriteLine($"Maximální číslo je {maxNumber} s pozicí {positionNumber}");
        }
    }
}