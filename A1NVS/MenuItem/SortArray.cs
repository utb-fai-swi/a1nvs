using System;
using System.Collections.Generic;
using System.Diagnostics;
using A1NVS.Utils;

namespace A1NVS.MenuItem
{
    /// <summary>
    ///  Menu item - sort array element
    /// </summary>
    public class SortArray : IMenuItem
    {
        private int _algorithmType;

        public SortArray(int algorithmType)
        {
            _algorithmType = algorithmType;
        }

        /// <summary>
        /// Sorting and listing numbers
        /// </summary>
        public void Main()
        {
            List<int> sortNumbers;

            Output.Info($"Řazení čísel pomocí {_algorithmType}. algoritmu");

            Stopwatch watch = Stopwatch.StartNew();

            switch (_algorithmType)
            {
                case 1:
                    sortNumbers = Container.GetSortArray().Shaker(Container.Numbers);
                    break;

                case 2:
                    sortNumbers = Container.GetSortArray().Bubble(Container.Numbers);
                    break;

                case 3:
                    sortNumbers = Container.GetSortArray().Selection(Container.Numbers);
                    break;

                default:
                    throw new IndexOutOfRangeException("Bad algorithm type");
            }

            watch.Stop();

            if (Container.Numbers.Count >= 100)
            {
                Output.Info("Výpis pole byl upraven, jelikož pole obsahuje velké množství čísel");
                Arrays.Write(sortNumbers, 50);
            }
            else
            {
                Arrays.Write(sortNumbers);
            }


            string watchElapsed = watch.Elapsed.TotalMilliseconds > 1000
                ? $"{Math.Round(watch.Elapsed.TotalSeconds, 2)}s"
                : $"{Math.Round(watch.Elapsed.TotalMilliseconds, 2)}ms";

            Output.Info($"Doba řazení: {watchElapsed}");
        }
    }
}