using System;

namespace A1NVS.MenuItem
{
    /// <summary>
    ///  Menu item - smallest element
    /// </summary>
    public class SmallestElement : IMenuItem
    {
        /// <summary>
        /// This is finding the smallest element and its position in array
        /// </summary>
        public void Main()
        {
            int minNumber = Container.Numbers[0];
            int positionNumber = 0;
            int minPosition = 0;

            do
            {
                int current = Container.Numbers[positionNumber];
                if (minNumber > current)
                {
                    minNumber = current;
                    minPosition = positionNumber;
                }

                positionNumber++;


            } while (positionNumber < Container.Numbers.Count);

            Console.WriteLine($"The smallest numbers is: {minNumber}");
            Console.WriteLine($"Its position is: {minPosition}");
        }
    }
}