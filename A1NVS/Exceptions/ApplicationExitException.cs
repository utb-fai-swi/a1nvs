using System;

namespace A1NVS.Exceptions
{
    /// <summary>
    /// Application exception used to terminate the application at the user's or program's request
    /// </summary>
    public class ApplicationExitException : Exception
    {
        public ApplicationExitException()
        {
        }

        public ApplicationExitException(string message)
            : base(message)
        {
        }

        public ApplicationExitException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}