using System;
using System.Collections.Generic;

namespace A1NVS
{
    /// <summary>
    /// Container for all classes
    /// </summary>
    public class Container
    {
        public static List<int> Numbers = new List<int>();

        public static bool Debug = false;
        
        private static SecureRandom _secureRandom;
        
        public static SecureRandom GetSecureRandom()
        {
            return _secureRandom ??= new SecureRandom();
        }
        
        private static Presenter _presenter;

        public static Presenter GetPresenter()
        {
            return _presenter ??= new Presenter();
        }

        private static ArgumentParser _argumentParser;
        
        public static ArgumentParser GetArgumentParser()
        {
            return _argumentParser ??= new ArgumentParser();
        }

        private static Utils.SortArray _sortArray;

        public static Utils.SortArray GetSortArray()
        {
            return _sortArray ??= new Utils.SortArray();
        }
    }
}