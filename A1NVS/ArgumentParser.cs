using System;
using System.IO;
using A1NVS.Exceptions;
using A1NVS.Utils;

namespace A1NVS
{
    /// <summary>
    /// Class for processing arguments
    /// </summary>
    public class ArgumentParser
    {
        /// <summary>
        /// It tries to find out whether it is an input file or a number field or nothing is entered
        /// </summary>
        /// <param name="args">Input parameters</param>
        public void TryParse(string[] args)
        {
            Container.Numbers.Clear();

            if (args.Length == 0)
            {
                Output.Info("Generování vstupních hodnot", true);
                EmptyInputArgument();
            }
            else if (args.Length == 1)
            {
                Output.Info("Načtení čísel z textu", true);
                FileInputArgument(args[0]);
            }
            else if (args.Length > 1)
            {
                Output.Info("Načtení hodnot přes vstupní parametry", true);
                ArrayInputArgument(args);
            }
        }

        /// <summary>
        /// Use a pseudo-random generator to generate 20 random characters
        /// </summary>
        private static void EmptyInputArgument()
        {
            do
            {
                Container.Numbers.Add(Container.GetSecureRandom().RandomInteger(-100, 100));
            } while (Container.Numbers.Count < 20);
        }

        /// <summary>
        /// Opens a file that is then processed
        /// </summary>
        /// <param name="filePath">File path</param>
        private static void FileInputArgument(string filePath)
        {
            if (!File.Exists(filePath))
            {
                Output.Error("Vstupní soubor neexistuje");
                Console.WriteLine("Program bude ukončen...");
                Console.ReadKey();
                throw new ApplicationExitException();
            }

            string[] lines = File.ReadAllLines(filePath);

            ArrayInputArgument(lines);
        }

        /// <summary>
        /// He takes a data field that first verifies whether it is a number, if so I fill it with a container
        /// </summary>
        /// <param name="data">Data input field</param>
        private static void ArrayInputArgument(string[] data)
        {
            foreach (string item in data)
            {
                if (int.TryParse(item, out int value))
                {
                    Container.Numbers.Add(value);
                }
            }

            if (Container.Numbers.Count < 1)
            {
                Output.Error("Pole čísel neobsahuje ani jednu platnou hodnotu");
                Console.WriteLine("Program bude ukončen...");
                Console.ReadKey();
                throw new ApplicationExitException();
            }
        }
    }
}