﻿using System;
using A1NVS.Exceptions;

namespace A1NVS
{
    /// <summary>
    /// Base of program
    /// </summary>
    class Program
    {
        /// <summary>
        /// Process parameters and start the program.
        /// </summary>
        /// <param name="args">args will be passed when starting this program</param>
        static void Main(string[] args)
        {
            try
            {
                Container.GetArgumentParser().TryParse(args);

                Container.GetPresenter().Run();
            }
            catch (ApplicationExitException)
            {
                Environment.Exit(0);
            }
        }
    }
}