﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace A1NVS.Utils
{
    /// <summary>
    /// Utility for working with the field
    /// </summary>
    class Arrays
    {
        /// <summary>
        /// Listing of the list of numbers on Console
        /// </summary>
        /// <param name="list">List of numbers</param>
        /// <param name="limit">Get first n numbers</param>
        public static void Write(List<int> list, int limit = 0)
        {
            ConsoleColor lastColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;

            if (limit > 0)
            {
                list = list.GetRange(0, limit);
                Console.WriteLine($"Výpis prvních {limit} čísel");
            }
            else
            {
                Console.WriteLine("Výpis seznamu čísel");
            }

            Console.ForegroundColor = lastColor;

            string output = String.Join(", ", list.ToArray());

            if (limit > 0)
            {
                output += ",......";
            }

            Console.WriteLine(output);
        }
    }
}