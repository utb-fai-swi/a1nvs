using System;

namespace A1NVS.Utils
{
    /// <summary>
    /// Utility for working with output values
    /// </summary>
    public class Output
    {
        /// <summary>
        /// Information message
        /// </summary>
        /// <param name="text">Message</param>
        /// <param name="debugMode">Print only when development mode is enabled</param>
        /// <param name="info">Text in square brackets</param>
        public static void Info(string text, bool debugMode = false, string info = "Informace")
        {
            Draw(info, ConsoleColor.Yellow, text, debugMode);
        }

        /// <summary>
        /// Error message
        /// </summary>
        /// <param name="text">Message</param>
        /// <param name="debugMode">Print only when development mode is enabled</param>
        /// <param name="info">Text in square brackets</param>
        public static void Error(string text, bool debugMode = false, string info = "Chyba")
        {
            Draw(info, ConsoleColor.Red, text, debugMode);
        }

        /// <summary>
        /// Print message
        /// </summary>
        /// <param name="firstPart">Text in square brackets</param>
        /// <param name="firstPartColor">The text color in brackets</param>
        /// <param name="secondPart">Message</param>
        /// <param name="debugMode">Print only when development mode is enabled</param>
        private static void Draw(string firstPart,
            ConsoleColor firstPartColor,
            string secondPart,
            bool debugMode = false)
        {
            if ((debugMode && Container.Debug) || debugMode == false)
            {
                Console.ResetColor();
                Console.ForegroundColor = firstPartColor;
                Console.Write($"[{firstPart}] ");
                Console.ResetColor();
                Console.WriteLine(secondPart);
            }
        }
    }
}