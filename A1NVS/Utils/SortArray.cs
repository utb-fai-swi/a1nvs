﻿using System;
using System.Collections.Generic;
using System.Text;

namespace A1NVS.Utils
{
    /// <summary>
    /// Sort list utility
    /// </summary>
    public class SortArray
    {
        /// <summary>
        /// Shaker short algoritm
        /// </summary>
        /// <param name="InputList">List of unsort numbers</param>
        /// <returns>List of sort numbers</returns>

        public List<int> Shaker(List<int> InputList)
        {
            int[] array = InputList.ToArray();

            for (int i = 0; i < array.Length / 2; i++)
            {
                bool swapped = false;
                for (int j = i; j < array.Length - i - 1; j++)
                {
                    if (array[j] < array[j + 1])
                    {
                        int tmp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = tmp;
                        swapped = true;
                    }
                }
                for (int j = array.Length - 2 - i; j > i; j--)
                {
                    if (array[j] > array[j - 1])
                    {
                        int tmp = array[j];
                        array[j] = array[j - 1];
                        array[j - 1] = tmp;
                        swapped = true;
                    }
                }
                if (!swapped) break;

            }

            return new List<int>(array);
        }

        /// <summary>
        /// Bubble sort algorithm
        /// </summary>
        /// <param name="InputList">List of unsort numbers</param>
        /// <returns>List of sort numbers</returns>
        public List<int> Bubble(List<int> InputList)
        {
            int[] array = InputList.ToArray();

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] > array[j])
                    {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }

            return new List<int>(array);
        }

        /// <summary>
        /// Selection sort algorithm
        /// </summary>
        /// <param name="InputList">List of numbers</param>
        /// <returns>List of sort numbers</returns>
        public List<int> Selection(List<int> InputList)
        {
            int[] array = InputList.ToArray();

            int temp, smallest;
            int n = array.Length;

            for (int i = 0; i < n - 1; i++)
            {
                smallest = i;

                for (int z = i + 1; z < n; z++)
                {
                    if (array[z] < array[smallest])
                    {
                        smallest = z;
                    }
                }

                temp = array[smallest];
                array[smallest] = array[i];
                array[i] = temp;
            }

            return new List<int>(array);
        }
    }
}