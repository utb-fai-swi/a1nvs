using System;

namespace A1NVS.Utils
{
    /// <summary>
    /// Utility for working with input values
    /// </summary>
    public class Input
    {
        /// <summary>
        /// Repeats the user input until the input is a number
        /// </summary>
        /// <param name="text">Input text</param>
        /// <param name="error">Error text</param>
        /// <returns>Integer</returns>
        public static int LoadInteger(string text, string error = "Zadaný vstup není číslo")
        {
            int output;
            bool parse;

            do
            {
                Console.Write(text);
                var input = Console.ReadLine();
                parse = int.TryParse(input, out output);

                if (!parse)
                {
                    Output.Error(error);
                }
            } while (!parse);

            return output;
        }
    }
}