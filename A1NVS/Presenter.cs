using System;
using System.Collections.Generic;
using A1NVS.Exceptions;
using A1NVS.MenuItem;
using A1NVS.Utils;

namespace A1NVS
{
    /// <summary>
    /// Data presentation tool
    /// </summary>
    public class Presenter
    {
        /// <summary>
        /// Start presenter
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public void Run()
        {
            DrawLogo();

            while (true)
            {
                CallMenu();

                Console.WriteLine();
                Output.Info("Pro pokračování stiskněte ENTER");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Draw logo
        /// </summary>
        /// <param name="withInfo">Include information</param>
        private static void DrawLogo(bool withInfo = true)
        {
            ConsoleColor color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("*************************");
            Console.WriteLine("*        MiniMax        *");
            Console.WriteLine("*************************");

            if (withInfo)
            {
                Console.ForegroundColor = Container.Numbers.Count >= 100 ? ConsoleColor.Red : ConsoleColor.Yellow;
                Console.WriteLine($"- Počet hodnot:\t {Container.Numbers.Count}");
            }

            Console.Write("\n");

            Console.ForegroundColor = color;
        }

        /// <summary>
        /// Draw menu
        /// </summary>
        private static void DrawMenu()
        {
            List<string> items = new List<string>();
            items.Add("Informace o nejmenším prvku");
            items.Add("Informace o největším prvku");
            items.Add("Seřadit pole  [1. algoritmus / Shaker]");
            items.Add("Seřadit pole  [2. algoritmus / Bubble]");
            items.Add("Seřadit pole  [3. algoritmus / Selection]");

            if (Container.Numbers.Count > 1000)
            {
                items.Add("Benchmark řadících algoritmů");
            }

            for (int i = 0; i < items.Count; i++)
            {
                Console.WriteLine($"{i + 1}. {items[i]}");
            }

            Console.WriteLine(new String('-', 35));

            Console.WriteLine("99. Vypsat pole");
            Console.WriteLine("0. Ukončit program");
        }

        private static void CallMenu()
        {
            DrawMenu();

            int option;
            bool optionOk;

            do
            {
                option = Input.LoadInteger("\nZadejte volbu: ");

                optionOk = (option >= 0 && option <= 6) || option == 99;

                if (!optionOk)
                {
                    Output.Error("Vstup může být od 1 do 5. Opakujte zadání.");
                }
            } while (!optionOk);

            Console.WriteLine();
            Output.Info($"Zadaná volba: {option.ToString()}");

            IMenuItem item;

            switch (option)
            {
                case 0:
                    throw new ApplicationExitException();

                case 1:
                    item = new SmallestElement();
                    break;

                case 2:
                    item = new BiggestElement();
                    break;

                case 3:
                case 4:
                case 5:
                    item = new MenuItem.SortArray(option - 2);
                    break;
                
                case 6:
                    item = new Benchmark();
                    break;

                case 99:
                    item = new ListingArray();
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            // Call main method
            item.Main();
        }
    }
}