# A1NVS

Není dovoleno odkudkoliv kopírovat fragmenty kódu. Ve všech příkladech je vyžadován vlastní algoritmus, na nějž bude řešitelský tým při prezentaci detailně dotazován. Neschopnost zodpovědět funkčnost jednotlivých částí zdrojového kódu bude chápáno jako plagiátorství a řešitelský tým tak nebude mít nárok na udělení zápočtu.

* Tým tvoří 2-4 studenti.
* Prezentace funkčního projektu se odehraje na cvičení v závěru semestru (viz "Přehled cvičení", až bude sestaven plán).
* Při udělování zápočtu bude kladen důraz na aktivní zapojení jednotlivců v týmech. Jinými slovy, zápočet nebude udělen členům týmů, kteří neprokáží aktivitu a neodprezentují svůj podíl na projektu. Není třeba žádné prezentace ve formě PPTX či PDF. Důraz je kladen na ústní podání řešení a použitých technologií.
* Délka prezentace: přibližně 15 minut.
* Pro zdárnou prezentaci projektu je nutné držet se všech následujících bodů:
    * Uveďte a stručně okomentujte (včetně subjektivních pohledů) IDE zvolené pro práci na projektu.
    * Při práci na projektu využijte systém pro správu verzí. Okomentujte svůj výběr a to, jak se vám s vámi vybraným systémem pracovalo, ukažte historii.
    * Představte řešení vámi vybraného problému (v libovolném programovacím jazyce).
    * Demonstrujte funkčnost automatického sestavení pomocí CMake, GNU Make, Ant, GNU Maven, Gradle či jiných nástrojů. Zdůvodněte svůj výběr a prezentujte své zkušenosti (v případě jazyka C, C++, Java).
    * Spusťte program a okomentujte jeho výstup.
    * Představte váš způsob začlenění testování do projektu.
    * Je nutné mít vaše programové řešení vhodně okomentované (všechny funkce, argumenty, případně třídy), tak aby se dala snadno automaticky generovat programová dokumentace. Tu necháte v průběhu prezentace pomocí Javadoc, Doxygen či obdobného nástroje vygenerovat a následně ji zobrazíte (v případě jazyka C, C++, C#, Objective-C, IDL, Java, VHDL, PHP, Python, Tcl, Fortran, and D).

## MINIMAX

VSTUP

* Program musí umět přijímat libovolně veliké pole celočíselných hodnot zadané
jako parametry, případně uložené v textovém souboru. V případě spuštění bez parametru se využije generátor pseudonáhodných čísel, který vygeneruje 20 prvků.

PŘÍKLAD SPUŠTĚNÍ

* minimax.exe 8763 35 765 1 -90
* minimax.exe soubor-s-cisly.txt
* minimax.exe

VÝSTUP

* Informace o nejmenším prvku (hodnota a pozice v poli)
* Informace o největším prvku (hodnota a pozice v poli)
* Seřazené pole (program musí mít implementaci pro tři různé algoritmy řazení a
uživatel bude v průběhu programu dotázán na volbu algoritmu)

## Pomocné videa
* [Vimeo Makefile](https://vimeo.com/377899954)

## Užitečné linky
* [Česká Git dokumentace](https://git-scm.com/book/cs/v2)
* [Řadící algoritmy](https://www.algoritmy.net/article/3/Bubble-sort)
* [Sorting Algorithms](https://www.toptal.com/developers/sorting-algorithms)