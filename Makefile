# Projekt:  MINIMAX
# Autor:    Radka Zaoralová, Marek Novák, Filip Šedivý
# Škola:    UTB / FAI SWI
# Předmět:  AUIUI/A1NVS

ProjectDir			= A1NVS
ProjectFile			= $(ProjectDir)/A1NVS.csproj
DocumentationDir	= Documentation

all:
	@echo "** Makefile **"
	@echo "cleanup restore build run docs"
	
cleanup:
	@dotnet clean $(ProjectDir)

restore: 
	@dotnet restore $(ProjectDir)

build: 
	@dotnet build $(ProjectDir) 

run:
	@dotnet run -p $(ProjectFile)

docs:
	@rm -rf $(DocumentationDir)
	@doxygen Doxyfile
	@open $(DocumentationDir)/index.html